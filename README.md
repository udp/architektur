# Architektur Urbane Datenplattform

## Beschreibung
Die hier bereitgestellten Projekte zur Urbanen Datenplattform sollen dazu beitragen, dass eine städteübergreifende Kollaboration ermöglicht wird und die Entwicklung von Kerninhalten und Datenflüssen interkommunal ausgetauscht werden können. Alle Kommunen sind dazu aufgerufen ihre eigenen Inhalte einzubringen und für andere bereitzustellen.
Im Allgemeinen besteht eine urbane Datenplattform aus unterschiedlichen Elementen, die je Kommune unterschiedlich ausgeprägt sein können. Eine Übersicht der verschiedenen Komponenten gibt die [DIN SPEC 91357](https://www.din.de/de/wdc-beuth:din21:281077528). Dieses Projekt dient dazu verschiedene Referenzarchitekturen von urbanen Datenplattformen zu sammeln. In untergeordneten Projekten können für die einzelnen Bausteine einer urbanen Datenplattform entsprechende Codes bereitgestellt werden und diese dienen somit dem Austausch und der gemeinsamen Entwicklung. Insbesondere die Anbindung bestehender kommunaler Fachverfahren und weit verbreiteten Sensoren oder auch bereits anderen öffentlichen Datenquellen bietet großes Potential für gemeinsame Entwicklungen. 
Ein Produkt von der Stange ist oft auf Basis von bestehenden Architekturen, Sicherheitsanforderungen und Betriebsmodellen nur schwer zu integrieren. Daher liegt der Fokus dieses Projektes darin zumindest die einzelnen Bausteine miteinander zu teilen und souveräne Entwicklung in den Kommunen und ihren IT-Dienstleistern zu fördern. 

## Nutzung und Beitrag
Jede Kommune, die aktuell an urbanen Datenplattformen arbeitet ist herzlich eingeladen ihre bestehende Referenzarchitektur hier darzustellen und sich an den Referenzarchitekturen anderer Kommunen zu inspirieren. Die Unterlagen können direkt in Unterordnern je Kommune abgelegt werden. Im Idealfall wird hierbei eine Orientierung an den in der DIN SPEC 91357 Capabilities erreicht, um eine Vergleichbarkeit herzustellen. Das ist aber kein Muss.

Im Verzeichnis `Vorlage` liegt ein Grundgerüst zur Beschreibung der eigenen Datenplattform bereit. Gerne die Powerpoint Vorlage verwenden, um die eigene Architektur darzustellen oder bereits vorhandene Darstellungen verwenden sowie die README anpassen und in einem eigenen Ordner ablegen.

## Project status
Active
