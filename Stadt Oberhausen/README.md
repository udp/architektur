# Beschreibung der Architektur in der Stadt Oberhausen

Die Datenplattform der Stadt Oberhausen besteht aus verschiedenen Kernelementen und nutzt wesentliche bereits vorhandene Bausteine aus etablierten Verwaltungseinheiten. Folgende Bausteine stellen die wesentlichen Funktionen einer urbanen Datenplattform sicher. Die Beschreibung erfolgt anhand des Capabilities Modells des DIN SPEC 91357 Standards.

![UDP Oberhausen](UDP_Architektur.jpg)

## 0 Field Equipment / Device Capabilities

Verschiedene Sensoren werden im Stadtgebiet eingesetzt, die in unterschiedlichen Anwendungsfällen eingesetzt werden.

## 1 Communications, Network & Transportat Capabilities

- **LoRaWAN** Netzwerk zur Datenübertragung
- öffentliche **LTE** Netze
- stadteigenes **Glasfasernetzwerk**

## 2 Device Asset Mgmt. & Operational Service Capabilities

- **[Firefly](https://firefly.iota.org/)** zur Datenverarbeitung der LoRaWAN Daten
- **[Niotix](https://info.niotix.io/)** zum Management der IoT Devices und Verarbeitung der LoRaWAN Payloads

## 3 Data Management & Analysis Capabilities

- **[NodeRed](https://nodered.org/)** zur Aufnahme und Verarbeitung von Daten aus verschiedenen Datenquellen
- **[DUVA](https://www.duva.de/)** Server auf Basis einer MS SQL Datenbank zur Datenhaltung, Datenstrukturen nach Möglichkeit auf Basis von FiWare Standards
- **[Grafana](https://grafana.com/)** zur Datenvisualisierung
- **[QGIS](https://qgis.org/de/site/)** zur Visualisierung von Georeferenzierten Daten
- **ArcGIS** zur Visualisierung von Georeferenzierten Daten

## 4 Integration & Orchestration Capabilities

- **Grafana** zur Verarbeitung von Alerts
- **DUVA** zur Veröffentlichung von Datenkatalogen intern wie extern (https://www.duva-server.de/OBIS) und Open Data Schnittstellen, Bedienung der Schnittstelle zu open.nrw
- **NodeRed** zur Bedienung von Schnittstellen

## 5 Generic City  / Community Capabilities

- Verschiedene Fachverfahren

## 6 Specific City / Community Capabilities

- Verschiedene Fachverfahren

## 7 Stakeholder Engagement & Collaboration Capabilities

- **[Beteiligungsportal](https://beteiligung.nrw.de/portal/oberhausen/startseite)** NRW zur Bürgerbeteiligung bzgl. Daten

## 8 Privacy & Security Capabilities

- **[keycloak](https://www.keycloak.org/)** zur Steuerung von Zugriffsrechten auf die urbane Datenplattform

## 9 Common Services Capabilities

- N.A.

